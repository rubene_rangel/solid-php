<?php
include "./IFly.php";
include "./ISwim.php";
include "./ICuack.php";

/**
 * Livkov Substitution
 */

/* class Duck
{
  public function fly()
  {
    // return "Fly";
  }

  public function swim()
  {
    // return "Swim";
  }

  public function cuack()
  {
    // return "Cuack";
  }
} */

class Duck implements IFly, ISwim, ICuack
{
  public function fly()
  {
    return "Fly Class Duck";
  }

  public function swim()
  {
    // 
  }

  public function cuack()
  {
    // 
  }
}
