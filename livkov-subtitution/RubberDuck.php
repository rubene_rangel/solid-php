<?php

/**
 * Livkov subsTitution
 */
include "./Duck.php";

/* class RubberDuck extends Duck
{
  public function fly()
  {
    throw new Error();
  }

  public function swim()
  {
    return "Le Swim";
  }

  public function cuack()
  {
    return "Le Cuack";
  }
} */

class RubberDuck implements ISwim, ICuack
{
  public function swim()
  {
    return "Le Interface Swim";
  }

  public function cuack()
  {
    return "Le Interface Cuack";
  }
}
