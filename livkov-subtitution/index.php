<?php

/**
 * Livkov Substitution
 */

include "./RubberDuck.php";
include "./DuckProcesser.php";
// include "./Duck.php";

$duck = new Duck;

$rubberDuck = new RubberDuck;

/* echo "\n" . $rubberDuck->swim() . "\n";

echo "\n" . $rubberDuck->cuack() . "\n"; */

$duckProcesser = new DuckProcesser;

echo "\n --- " . $duckProcesser->makeDucksFly($duck) . "\n";

// echo "\n --- " . $duckProcesser->makeDucksFly($rubberDuck);
