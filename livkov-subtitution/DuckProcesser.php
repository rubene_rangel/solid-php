<?php
class DuckProcesser
{
  public function makeDucksFly(IFly $duck)
  {
    return $duck->fly();
  }
}