<?php

class Main
{
  public function handleFile($file)
  {
    //get all methods
    $input['file'] = $file;

    // Add to database
    return 'Added to DB';
  }
}

class Child extends Main
{
  public function handleFile($file)
  {
    if (!is_object($file)) {
      var_dump('Dead');
      die;
    }

    // Add to database
  }
}

class Other extends Child
{
  public function handleFile($file)
  {
    return parent::handleFile($file);
  }
}

$other = new Other();

echo $other->handleFile('Hello') . "\n";
