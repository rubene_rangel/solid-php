<?php

/**
 * Single Responsability
 */

namespace Acme;

class ImageHandler
{
  protected $image;

  public function __construct($image)
  {
    $this->image = $image;
  }

  public function validate()
  {
    // vaidate image
  }

  public function resize()
  {
    // resize image 
  }

  // This methods move to Own class
  /* public function upload()
  {
    // logic upload
  }*/
}
