<?php

/**
 * Single Responsability
 */
class PasswordEncrypter
{
  static function encryted($password)
  {
    $encriptedPassword = password_hash($password, PASSWORD_DEFAULT);

    return $encriptedPassword;
  }
}
