<?php

/**
 * Hight level code is not concerned with the details
 * El codigo de alto nivel no tiene que ver con los detalles
 */

/**
 * Low level is concerned with the details
 */

interface PokemonBattleInterface
{
  public function attack();

  public function battle();

  public function returnToPokeball();
}

class Charmander implements PokemonBattleInterface
{
  public function attack()
  {
    # code...
  }

  public function returnToPokeball()
  {
    # code...
  }

  public function battle()
  {
    $this->attack();
    $this->returnToPokeball();
  }
}

class Ash
{
  public function iChoseYou(PokemonBattleInterface $pokemon)
  {
    $pokemon->battle();

    return "Pokemon fought bravely then returned to its pokeball";
  }
}

$trainer = new Ash();
$pokemon = new Charmander();

echo $trainer->iChoseYou($pokemon) . "\n";
