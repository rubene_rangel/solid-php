<?php

namespace Acme;

class TeaMaker
{
  // protected $reval;
  protected $instructions;

  public function __construct(InstructionsInterface $instructions)
  {
    $this->instructions = $instructions;
  }

  public function makeTea()
  {
    return $this->instructions->makeTeaInstructions();
  }
}
