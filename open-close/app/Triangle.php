<?php

namespace Acme;

use Acme\IShapeInterface;

/**
 * Open / Close
 */

class Triangle implements IShapeInterface
{
  protected $width;
  protected $heigth;

  public function __construct($width, $heigth)
  {
    $this->width = $width;
    $this->heigth = $heigth;
  }

  public function area()
  {
    return $this->width * $this->heigth / 2;
  }
}
