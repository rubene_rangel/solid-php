<?php

namespace Acme;

interface InstructionsInterface
{
  public function makeTeaInstructions();
}
