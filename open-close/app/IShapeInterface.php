<?php

namespace Acme;

interface IShapeInterface
{
  public function area();
}
