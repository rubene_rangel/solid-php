<?php

namespace Acme;

use Acme\IShapeInterface;

class AreaCalculator
{
  public function computedArea(IShapeInterface $shapes)
  {
    $area = 0;

    $area = $shapes->area();

    return $area;
  }
}
