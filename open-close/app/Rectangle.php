<?php

namespace Acme;

use Acme\IShapeInterface;

/**
 * Open / Close
 */

class Rectangle implements IShapeInterface
{
  protected $width;
  protected $heigth;

  public function __construct($width, $heigth)
  {
    $this->width = $width;
    $this->heigth = $heigth;
  }

  public function area()
  {
    return $this->width * $this->heigth;
  }
}
