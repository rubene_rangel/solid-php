<?php

namespace Acme;

class Reval implements InstructionsInterface
{
  public function makeTeaInstructions()
  {
    return '1 red bush tea bag, no sugar and no milk';
  }
}
