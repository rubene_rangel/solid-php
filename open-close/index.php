<?php
require './vendor/autoload.php';

use Acme\AreaCalculator;
use Acme\Reval;
use Acme\TeaMaker;
use Acme\Jack;
use Acme\Rectangle;
use Acme\Triangle;

/**
 * TEA
 */

$reval = new Reval;

$jack = new Jack;

$teaMaker = new TeaMaker($reval);

echo "\nReval's tea - " . $teaMaker->makeTea() . "\n";

$teaMaker = new TeaMaker($jack);

echo "\nJack's tea - " . $teaMaker->makeTea() . "\n";

/**
 * SHAPES
 */

$rectangle = new  Rectangle(3, 2);

$triangle = new  Triangle(3, 2);

$areaCalculator = new AreaCalculator();

echo "\n Area Rectangle is: " . $areaCalculator->computedArea($rectangle) . "\n";
echo "\n Area Triangle is: " . $areaCalculator->computedArea($triangle) . "\n";
