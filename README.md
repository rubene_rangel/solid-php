# SOLID #

SOLID principles, standards or recommendations for programming our systems.

* More maintainable code.
* Easy to change.
* Facilitate the incorporation of new functionalities.
* Readable and easy to understand code