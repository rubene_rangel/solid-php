<?php

interface PokemonInterface
{
  public function defend();

  public function attack();

  public function returnToPokeball();
}

class Charmander implements PokemonInterface
{
  public function defend()
  {
  }

  public function attack()
  {
  }

  public function returnToPokeball()
  {
  }
}

class Metapod implements PokemonInterface
{
  public function defend()
  {
  }

  public function attack()
  {
    // TODO: Implement attack () method.
  }

  public function returnToPokeball()
  {
  }
}

class Ash
{
  public function iChoseYou(PokemonInterface $pokemon)
  {
    $pokemon->defend();
    $pokemon->attack();
    $pokemon->returnToPokeball();
  }
}
