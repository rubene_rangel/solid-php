<?php

class Charmander
{
  public function defend()
  {
    // 
  }

  public function attack()
  {
    // 
  }

  public function returnToPokeball()
  {
    // 
  }
}

class Ash
{
  public function iChoseYou(Charmander $pokemon)
  {
    $pokemon->defend();
    $pokemon->attack();
    $pokemon->returnToPokeball();
  }
}
