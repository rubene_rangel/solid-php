<?php

interface PokemonBattleInterface
{
  public function battle();
}

interface PokemonStandardInterface
{
  public function defend();

  public function returnToPokeball();
}

interface PokemonAttackerInterface
{
  public function attack();
}

class Charmander implements PokemonAttackerInterface, PokemonStandardInterface, PokemonBattleInterface
{
  public function defend()
  {
  }

  public function returnToPokeball()
  {
  }

  public function attack()
  {
  }

  public function battle()
  {
    $this->defend();
    $this->attack();
    $this->returnToPokeball();
  }
}

class Metapod implements PokemonStandardInterface, PokemonBattleInterface
{
  public function defend()
  {
  }

  public function returnToPokeball()
  {
  }

  public function battle()
  {
    $this->defend();
    $this->returnToPokeball();
  }
}

class Ash
{
  public function iChoseYou(PokemonBattleInterface $pokemon)
  {
    $pokemon->battle();
  }
}
