<?php
interface PokemonBattleInterface
{
  public function battle();
}

interface PokemonStandardInterface
{
  public function defend();

  public function returnToPokeball();
}

interface PokemonAttackerInterface
{
  public function attack();
}

abstract class PokemonFireType
{
  public function weakness()  //debilidad
  {
    var_dump('Attack decreased'); //disminuido
  }
}

class Charmander extends PokemonFireType implements PokemonAttackerInterface, PokemonStandardInterface, PokemonBattleInterface
{
  public function defend()
  {
    // defend.
  }

  public function returnToPokeball()
  {
    // return to pokeball.
  }

  public function attack()
  {
    // attack.
  }

  public function battle()
  {
    $this->defend();
    $this->attack();
    $this->returnToPokeball();

    return 'battle';
  }
}

class Metapod implements PokemonStandardInterface, PokemonBattleInterface
{
  public function defend()
  {
  }

  public function returnToPokeball()
  {
  }

  public function battle()
  {
    $this->defend();
    $this->returnToPokeball();
  }
}

class Ash
{
  public function iChoseYou(PokemonBattleInterface $pokemon)
  {
    $pokemon->weakness();

    return $pokemon->battle();
  }
}

$trainer = new Ash();
$pokemon = new Metapod();

echo $trainer->iChoseYou($pokemon) . "\n";
