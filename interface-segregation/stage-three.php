<?php

interface PokemonStandardInterface
{
  public function defend();

  public function returnToPokeball();
}

interface PokemonAttackerInterface
{
  public function attack();
}

class Charmander implements PokemonAttackerInterface, PokemonStandardInterface
{
  public function defend()
  {
  }

  public function returnToPokeball()
  {
  }

  public function attack()
  {
  }
}

class Metapod implements PokemonStandardInterface
{
  public function defend()
  {
  }

  public function returnToPokeball()
  {
  }
}

class Ash
{
  public function iChoseYou(PokemonStandardInterface $pokemon)
  {
    $pokemon->defend();
    $pokemon->attack(); // Can we attack? Shoul we use an if statement to detect instance?
    $pokemon->returnToPokeball();
  }
}
